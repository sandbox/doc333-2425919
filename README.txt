README.txt
----------

INTRODUCTION
------------
The Menu Link Entity module allows you to create
a menu link at the creation of any entity.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/doc333/2425919

* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/node/add/project-issue/2425919
   
RECOMMENDED MODULES
-------------------
 * Entity Construction Kit (https://www.drupal.org/project/eck)
 
INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure the available menus for each entity in the Drupal administration, in structure and Menu link entity (admin/structure/menu_entity).
 
Current maintener:
 * Loïc Mangold (doc333) - https://www.drupal.org/user/3136385