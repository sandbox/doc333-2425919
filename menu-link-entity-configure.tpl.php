<?php
/**
 * @file
 * Display a list of links to configure the available menus of the entities.
 *
 * Available variables:
 * - $links: An array of links to display.
 */
?>
<div class='menu-link-entity-configure'>
  <?php foreach ($links as $link): ?>
    <p>
      <?php print render($link); ?>
    <p>
  <?php endforeach; ?>
</div>
